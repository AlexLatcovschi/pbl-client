import {Component, OnInit} from '@angular/core';
import {PushNotificationService} from './services/push-notifications.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Divysion';

  constructor(private pushNotificationService: PushNotificationService) {

  }

  ngOnInit(): void {
    this.pushNotificationService.init();
  }
}
