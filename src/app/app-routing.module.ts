import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {appLayoutRoutes} from './app-layout/app-layout.route';

@NgModule({
  imports: [
    RouterModule.forRoot(
      [
        {
          path: '',
          children: appLayoutRoutes,
        },
      ]
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
