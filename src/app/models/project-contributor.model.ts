import {ProjectModel} from './project';
import {UserModel} from './user.model';
import {ContributionModel} from './contribution.model';

export class ProjectContributorModel {
  public id: number;
  public project?: ProjectModel;
  public user?: UserModel;
  public hourPrice?: string;
  public investedMoney?: string;
  public position?: string;
  public actions?: number;
  public snacks?: number;
  public joined?: Date;
  public workedTime?: string;
  public contributions?: ContributionModel[];
  public color?: string;
  public status?: string;

  constructor() {
  }

  static mapData(data): ProjectContributorModel {
    if (data) {
      const contributor = new ProjectContributorModel();
      contributor.id = data.id;
      if (data.project) {
        contributor.project = ProjectModel.mapData(data.project);
      }
      if (data.user) {
        contributor.user = UserModel.mapData(data.user);
      }
      contributor.hourPrice = data.hourPrice;
      contributor.investedMoney = data.investedMoney;
      contributor.position = data.position;
      contributor.actions = data.actions;
      contributor.snacks = data.snacks;
      contributor.joined = data.joined;
      contributor.workedTime = data.workedTime;
      if (data.contributions) {
        contributor.contributions = ContributionModel.mapMultipleData(data.contributions);
      }
      contributor.color = data.color;
      contributor.status = data.status;
      return contributor;
    }
  }

  static mapMultipleData(data) {
    return data.map(el => {
      return ProjectContributorModel.mapData(el);
    });
  }
}
