import {UserModel} from './user.model';

export class InvestmentModel {
  public id: string;
  public name: string;
  public contributors: UserModel[];
  public date: string;


  constructor() {
  }
}
