import {ProjectContributorModel} from './project-contributor.model';
import {IMAGE_PREFIX} from '../app.constants';

export class ProjectModel {
  public id: number;
  public name: string;
  public slug: string;
  public type: string;
  public team?: ProjectContributorModel[];
  public imageUrl?: string;

  constructor() {
  }

  static mapData(data): ProjectModel {
    if (data) {
      const project = new ProjectModel();
      project.id = data.id;
      project.name = data.name;
      project.slug = data.slug;
      project.type = data.type;
      project.team = [];
      if (data.team) {
        project.team = ProjectContributorModel.mapMultipleData(data.team);
      }
      project.imageUrl = 'https://x.kinja-static.com/assets/images/logos/placeholders/default.png';
      if (data.imageUrl) {
        if (data.imageUrl.startsWith('public')) {
          project.imageUrl = IMAGE_PREFIX + data.imageUrl;
        } else {
          project.imageUrl = data.imageUrl;

        }
      }
      return project;
    }
  }

  static mapMultipleData(data) {
    return data.map(el => {
      return ProjectModel.mapData(el);
    });
  }
}
