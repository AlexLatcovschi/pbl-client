import {UserModel} from './user.model';
import {ProjectModel} from './project';

export class ContributionModel {
  id?: number;
  time?: number;
  money?: number;
  description?: string;
  createdAt?: Date;
  user?: UserModel;
  project?: ProjectModel;

  constructor() {

  }

  static mapData(data): ContributionModel {
    if (data) {
      const contribution = new ContributionModel();
      contribution.id = data.id;
      contribution.time = data.time;
      contribution.money = data.money;
      contribution.description = data.description;
      contribution.createdAt = data.createdAt;
      if (data.user) {
        contribution.user = UserModel.mapData(data.user);
      }
      if (data.project) {
        contribution.project = ProjectModel.mapData(data.project);
      }
      return contribution;
    }
  }

  static mapMultipleData(data) {
    return data.map(el => {
      return ContributionModel.mapData(el);
    });
  }
}
