import {ProjectModel} from './project';

export class UserModel {
  public id?: number;
  public firstName?: string;
  public lastName?: string;
  public pictureUrl?: string;
  public description?: string;
  public password?: string;
  public email?: string;
  public activated?: boolean;
  public roles?: Array<string>;
  public projects?: ProjectModel[];

  constructor() {
  }

  static mapData(data) {
    const user = new UserModel();
    if (data) {
      if (data.id) {
        user.id = data.id;
      }
      user.firstName = data.firstName ? data.firstName : '';
      user.lastName = data.lastName ? data.lastName : '';
      user.pictureUrl = data.pictureUrl ? data.pictureUrl : '';
      user.description = data.description ? data.description : '';
      if (data.activated) {
        user.activated = data.activated;
      }
      if (data.email) {
        user.email = data.email;
      }

      if (data.roles) {
        user.roles = data.roles;
      }
    }
    return user;
  }

  static mapMultipleUser(data) {
    return data.map(el => {
      return UserModel.mapData(el);
    });
  }
}
