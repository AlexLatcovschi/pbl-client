import {UserModel} from './user.model';
import {NotificationEventEnum} from './NotificationEventEnum';
import {ProjectModel} from './project';

export class NotificationModel {
  public id?: number;
  public event?: NotificationEventEnum;
  public content?: string;
  public seen?: boolean;
  public author?: UserModel;
  public recipient?: UserModel;
  public createdAt?: string;
  public projectId?: number;
  public fullProject?: ProjectModel;



  constructor() {
  }
}
