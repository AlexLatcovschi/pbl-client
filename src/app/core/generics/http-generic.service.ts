import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {createRequestOption} from '../../shared-components/util/request-util';
import {SERVER_API_URL} from '../../app.constants';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export abstract class HttpGenericService<T> {

  public abstract resourceUrl;

  constructor(public http: HttpClient) {
  }

  public create(user: T): Observable<T> {
    return this.http.post<T>(this.getResourceUrl(), user);
  }

  public update(user: T, id: number): Observable<T> {
    return this.http.put<T>(`${this.getResourceUrl()}/${id}`, user);
  }

  public find(id: number, params?: any): Observable<T> {
    return this.http.get<T>(`${this.getResourceUrl()}/${id}`, {params});
  }

  public query(req?: any): Observable<HttpResponse<T[]>> {
    const options = createRequestOption(req);
    return this.http.get<T[]>(this.getResourceUrl(), {params: options, observe: 'response'});
  }

  public delete(id: number): Observable<any> {
    return this.http.delete(`${this.getResourceUrl()}/${id}`);
  }

  getResourceUrl(): string {
    return `${environment.serverUrl}/api/${this.resourceUrl}`;
  }
}
