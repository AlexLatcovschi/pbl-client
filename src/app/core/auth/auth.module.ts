import { NgModule } from '@angular/core';
import { AuthService } from './auth.service';
import { ActivatedUserGuard } from './activated-user.guard';
import {AuthGuard} from '../guards/auth.guard';

@NgModule({
  declarations: [],
  imports: [],
  providers: [AuthService, AuthGuard, ActivatedUserGuard],
  bootstrap: [],
  exports: [],
})
export class AuthModule {}
