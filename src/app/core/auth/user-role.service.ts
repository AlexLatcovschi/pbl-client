import { root } from 'rxjs/internal-compatibility';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserDataService } from './user-data.service';

@Injectable({
  providedIn: 'root',
})
export class UserRoleService {
  roles$ = new BehaviorSubject(false);

  constructor(private userData: UserDataService) {}

  getRoles(authorities): Observable<any> {
    this.fetchUser(authorities);
    return this.roles$.asObservable();
  }

  fetchUser(authorities): void {
    this.userData.getUser().subscribe(
      user => {
        if (user) {
          const aa = user.roles.includes(authorities);

          this.roles$.next(user.roles);
          this.roles$.complete();
        } else {
          this.roles$.next(false);
          this.roles$.complete();
        }
      },
      error => {
        this.roles$.error(error);
      },
    );
  }
}
