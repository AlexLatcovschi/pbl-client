import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { APP_AUTH_TOKEN_KEY } from '../../app.constants';
import { CookieService } from 'ngx-cookie-service';
import {UserModel} from '../../models/user.model';

@Injectable({ providedIn: 'root' })
export class UserDataService {
  data;
  userSubject: BehaviorSubject<any>;
  loading = false;

  constructor(private http: HttpClient,
              private cookieService: CookieService) {
    this.userSubject = new BehaviorSubject(this.data);
  }

  getUser(): Observable<any> {
    if (this.data) {
      return this.userSubject.asObservable();
    }

    if (this.cookieService.get(APP_AUTH_TOKEN_KEY)) {
      if (!this.data && !this.loading) {
        this.loading = true;
        this.getUserProfile().subscribe(userData => {
          this.loading = false;
          this.data = userData;
          this.userSubject.next(this.data);
          return this.userSubject.asObservable();
        });
      }

      return this.userSubject.asObservable();
    }

    this.userSubject.next(null);
    return this.userSubject.asObservable();
  }

  getUserById(userId) {
    return this.http.get(`${environment.serverUrl}/api/users/${userId}`).pipe(
      map((res: any) => {
        return UserModel.mapData(res);
      }),
    );
  }

  clearUser() {
    this.data = null;
    this.userSubject.next(null);
  }

  getUserProfile(): Observable<UserModel> {
    if (this.data) {
      return this.userSubject.asObservable();
    }
    return this.http.get(`${environment.serverUrl}/api/profile`).pipe(
      map((res: any) => {
        return UserModel.mapData(res);
      }),
    );
  }

  manageUserStatus(userId, status) {
    let changedStatus;
    if (status === 'block') {
      changedStatus = 'BLOCKED';
    } else if (status === 'unblock') {
      changedStatus = 'ACTIVE';
    }
    if (changedStatus) {
      return this.http.put(`${environment.serverUrl}/api/users/${userId}/${changedStatus}`, {});
    }
  }
}
