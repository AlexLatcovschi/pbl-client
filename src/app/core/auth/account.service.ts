import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { catchError, map } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { UserDataService } from './user-data.service';
import {UserModel} from '../../models/user.model';
import {PasswordModel} from '../../models/password.model';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {SERVER_API_URL} from '../../app.constants';

@Injectable({
  providedIn: 'root',
})
export class AccountService {
  constructor(private http: HttpClient, private auth: AuthService, private userDataService: UserDataService) {}

  accountRecoveryInit(email) {
    return this.http.post(`${environment.serverUrl}/api/profile/reset-password/init`, email);
  }

  accountRecoveryFinish(userData) {
    return this.http.post(`${environment.serverUrl}/api/profile/reset-password/finish`, userData).pipe(
      map((res: any) => {
        const token = res.token;

        if (token) {
          this.auth.storeAuthenticationToken(token);
          return token;
        }

        return undefined;
      }, catchError(err => throwError(err))),
    );
  }

  changePassword(password: PasswordModel) {
    const userPassword = {
      newPassword: password.new,
      oldPassword: password.old,
    };

    return this.http.post(`${environment.serverUrl}/api/profile/change-password`, userPassword);
  }

  updateUserData(user: UserModel) {
    return this.http.put(`${environment.serverUrl}/api/profile`, user);
  }

  resendEmailConfirmation() {
    return this.http.post(`${environment.serverUrl}/api/resend-email`, {});
  }

  activateEmail(userData) {
    const userConfirmationInfo = {
      email: userData.email,
      key: userData.key,
    };
    return this.http.post(`${environment.serverUrl}/api/activate-email`, userConfirmationInfo).pipe(
      map((res: any) => {
        const token = res.token;
        if (token) {
          this.auth.storeAuthenticationToken(token);
          return token;
        }
        return undefined;
      }),
      catchError(err => throwError(err)),
    );
  }

  uploadImage(image) {
    const formData = new FormData();
    formData.append('image', image);
    return this.http.post(`${environment.serverUrl}/api/upload/users/image`, formData, {
      reportProgress: true,
      observe: 'events',
    });
  }
  get(): Observable<HttpResponse<Account>> {
    return this.http.get<Account>(SERVER_API_URL + 'api/profile', {observe: 'response'});
  }
}
