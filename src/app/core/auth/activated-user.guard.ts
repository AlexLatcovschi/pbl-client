import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { UserDataService } from './user-data.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ActivatedUserGuard implements CanActivate {
  constructor(private userDataService: UserDataService) {}

  canActivate(): Observable<boolean> {
    return this.userDataService.getUser().pipe(map((res: any) => (res ? res.activated : null)));
  }
}
