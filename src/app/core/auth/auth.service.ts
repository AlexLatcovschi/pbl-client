import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {LocalStorageService} from 'ngx-webstorage';
import {Observable, throwError} from 'rxjs';
import {Router} from '@angular/router';
import {UserDataService} from './user-data.service';
import {catchError, map} from 'rxjs/operators';
import {CookieService} from 'ngx-cookie-service';
import {APP_AUTH_TOKEN_KEY} from '../../app.constants';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({providedIn: 'root'})
export class AuthService {
  jwtHelper = new JwtHelperService();

  constructor(
    private http: HttpClient,
    private localStorage$: LocalStorageService,
    private router: Router,
    private userDataService: UserDataService,
    private cookieService: CookieService,
  ) {
  }

  getToken() {
    return this.cookieService.get(APP_AUTH_TOKEN_KEY);
  }

  isLoggedIn() {
    return Boolean(this.getToken());
  }

  login(credentials) {
    const userCredentials = {
      email: credentials.email,
      password: credentials.password,
    };
    return this.http.post<any>(`${environment.serverUrl}/api/auth/local`, userCredentials)
      .pipe(
        map(({token}) => {
          if (token) {
            this.storeAuthenticationToken(token);
            this.userDataService.getUser();
            return token;
          }
          return undefined;
        }),
        catchError(err => throwError(err)),
      );
  }

  register(data) {
    return this.http.post(`${environment.serverUrl}/api/register`, data);
  }

  logout(goHome?) {
    this.localStorage$.clear('logInLinkRedirect');
    this.cookieService.delete(APP_AUTH_TOKEN_KEY);
    this.userDataService.clearUser();
    if (goHome) {
      this.router.navigate(['/']);
    }
  }

  storeAuthenticationToken(jwt: string) {
    // this.localStorage$.store(APP_AUTH_TOKEN_KEY, jwt);
    this.cookieService.set(APP_AUTH_TOKEN_KEY, jwt, this.jwtHelper.getTokenExpirationDate(jwt));
  }

  loginWithToken(jwt: string) {
    return new Observable(observer => {
      if (jwt) {
        this.userDataService.getUser();
        this.storeAuthenticationToken(jwt);
        observer.complete();
      }
    });
  }

  getTokenExpireDate(): Date {
    return this.jwtHelper.getTokenExpirationDate(this.getToken());
  }
}
