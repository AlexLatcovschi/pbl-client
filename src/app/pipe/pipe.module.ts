import {NgModule} from '@angular/core';
import {FirstLetterPipe} from './first-letter.pipe';
import {ReadMorePipe} from './read-more.pipe';
import {SafeHtmlPipe} from './safe-html.pipe';
import {HtmlToText} from './htmlToText';
import {TimePipe} from './time.pipe';

@NgModule({
  declarations: [
    FirstLetterPipe,
    ReadMorePipe,
    SafeHtmlPipe,
    HtmlToText,
    TimePipe
  ],
  imports: [],
  providers: [],
  exports: [
    FirstLetterPipe,
    ReadMorePipe,
    SafeHtmlPipe,
    HtmlToText,
    TimePipe
  ],
})
export class PipeModule {
}
