import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'time',
})
export class TimePipe implements PipeTransform {
  constructor() {
  }

  transform(value: any, args?: any): any {
    const time = moment(value);
    let result;
    result = time.format('DD MMM YYYY');
    if (args === 'hours') {
      result = time.format('DD MMM YYYY HH:mm');
    }
    return result;
  }
}
