import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'HtmlToText' })
export class HtmlToText implements PipeTransform {
  transform(value) {
    return value ? String(value).replace(/<[^>]+>/gm, '') : '';
  }
}
