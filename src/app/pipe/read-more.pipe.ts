import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'readMore',
})
export class ReadMorePipe implements PipeTransform {
  transform(value: string, characters?: any): any {
    if (characters === 'full' || value.length < characters) {
      return value;
    } else if (typeof characters === 'number') {
      return value.slice(0, characters) + '...';
    }
    return null;
  }
}
