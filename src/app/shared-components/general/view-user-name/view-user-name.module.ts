import {NgModule} from '@angular/core';
import {ViewUserNameComponent} from './view-user-name.component';
import {CommonModule} from '@angular/common';
import {PipeModule} from '../../../pipe/pipe.module';

@NgModule({
  imports: [
    CommonModule,
    PipeModule
  ],
  declarations: [
    ViewUserNameComponent,
  ],
  exports: [
    ViewUserNameComponent
  ],
})
export class ViewUserNameModule {
}
