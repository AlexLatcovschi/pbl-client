import {Component, Input} from '@angular/core';
import {UserModel} from '../../../models/user.model';

@Component({
  selector: 'app-view-user-name',
  template: `
      {{value.firstName}}
      <ng-container *ngIf="!short">
          {{value.lastName}}
      </ng-container>

      <ng-container *ngIf="short">
          {{(value.lastName | firstLetter) + '.'}}
      </ng-container>
  `
})
export class ViewUserNameComponent {
  @Input() public value: UserModel;
  @Input() public short: boolean;
}
