import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {ModalComponent} from './modal.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ToastrModule} from 'ngx-toastr';

@NgModule({
  imports: [CommonModule, RouterModule, ReactiveFormsModule, FormsModule, ToastrModule],
  declarations: [
    ModalComponent,
  ],
  exports: [
    ModalComponent
  ],
})
export class ModalModule {
}
