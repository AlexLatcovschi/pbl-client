import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PasswordModel} from '../../../models/password.model';
import {MustMatch} from '../../../Validators/match-validator';
import {AccountService} from '../../../core/auth/account.service';
import {ToastrService} from 'ngx-toastr';
import {ProjectModel} from '../../../models/project';
import {ProjectService} from '../../../services/project.service';
import {Router} from '@angular/router';
import {ContributionModel} from '../../../models/contribution.model';
import {ProjectContributorModel} from '../../../models/project-contributor.model';
import {ProjectContributorService} from '../../../services/project-contributor.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  @Input() template;
  @Input() params;
  @Output() showModal = new EventEmitter();
  @Output() confirmed = new EventEmitter();
  password: PasswordModel;
  hideModal: boolean;
  submitted;
  registerForm: FormGroup;
  contributionData = false;
  contributionType;
  contributionForm: ContributionModel;
  project: ProjectModel;
  allProjectTypes;
  error;
  contributor: ProjectContributorModel;
  allContributorStatuses;

  constructor(
    private formBuilder: FormBuilder,
    private accountService: AccountService,
    private toastr: ToastrService,
    private projectService: ProjectService,
    private router: Router,
    private contributorService: ProjectContributorService
  ) {
  }

  get f() {
    return this.registerForm.controls;
  }

  ngOnInit() {
    this.password = new PasswordModel();
    this.contributionForm = new ContributionModel();
    this.project = new ProjectModel();
    if (this.template === 'changePassword') {
      this.registerForm = this.formBuilder.group(
        {
          oldPassword: ['', [Validators.required, Validators.pattern('^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$')]],
          newPassword: ['', [Validators.required, Validators.pattern('^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$')]],
          confirmPassword: ['', Validators.required],
        },
        {
          validators: MustMatch('newPassword', 'confirmPassword'),
        },
      );
    } else if (this.template === 'chooseContribution') {
      this.registerForm = this.formBuilder.group(
        {
          timeContribution: '',
          moneyContribution: '',
          descriptionContribution: ''
        }
      );
    } else if (this.template === 'createProject') {
      this.registerForm = this.formBuilder.group(
        {
          projectName: ['', Validators.required],
          projectType: ['', Validators.required],
        }
      );
      this.getProjectTypes();
    } else if (this.template === 'editContributor') {
      this.registerForm = this.formBuilder.group(
        {
          hourPrice: ['', Validators.required],
          position: ['', Validators.required],
          color: ['', Validators.required],
          status: ['', Validators.required],
        }
      );
      this.getContributorStatuses();
      this.getContributor();
    }
  }

  actionsModal() {
    console.log('actions modal');
    this.hideModal = true;
    this.showModal.emit(!this.hideModal);
  }

  clickedYes() {
    if (this.template === 'changePassword') {
      this.changePassword();
      this.submitted = true;
      this.confirmed.emit(true);
      this.actionsModal();
    } else if (this.template === 'chooseContribution') {
      this.addContribution();
      this.submitted = true;
      this.actionsModal();
    } else if (this.template === 'createProject') {
      this.error = (this.f.projectName.errors ? this.f.projectName.errors.required : false);
      this.submitted = true;
      if (!this.error) {
        this.createProject();
      } else {
        this.toastr.error('Please fill in the name of the company', 'Error');
      }
    } else if (this.template === 'editContributor') {
      const hourPriceError = (this.f.hourPrice.errors ? this.f.hourPrice.errors.required : false);
      const positionError = (this.f.position.errors ? this.f.position.errors.required : false);
      const colorError = (this.f.color.errors ? this.f.color.errors.required : false);
      this.error = (hourPriceError && positionError && colorError);
      this.submitted = true;
      if (!this.error) {
        this.updateContributor();
      } else {
        this.toastr.error('Please enter all datas in the fields', 'Error');
      }
    }
  }

  clickedNo() {
    if (this.template === 'changePassword') {
      this.confirmed.emit(false);
      this.actionsModal();
    } else if (this.template === 'chooseContribution') {
      this.actionsModal();
    }
  }

  changePassword() {
    this.accountService.changePassword(this.password).subscribe(
      () => {
        this.toastr.success('Password has been changed successfully!', 'Success');
      },
      error => {
        this.toastr.error(error.error.message, 'Error');
      },
    );
  }

  addContribution(userId?: number) {
    console.log(this.params);
    const params = {
        contributorId: null,
        projectId: this.params.projectId,
        data: this.contributionForm,
      }
    ;
    if (userId) {
      params.contributorId = userId;
    }
    this.contributorService.addContribution(params).subscribe(data => {
      console.log(data);
    }, error1 => {
      this.toastr.error(error1.error.message, 'Error');
    }, () => {
      this.toastr.success('You added contribution', 'Success');
    });
  }

  createProject() {
    this.projectService.create(this.project).subscribe((data) => {
      this.project = ProjectModel.mapData(data);
    }, (error) => {
      this.toastr.error(error.error.message, 'Error');
    }, () => {
      this.toastr.success('Company was created successfully!', 'Success!');
      this.actionsModal();
      this.router.navigate(['/app/project/', this.project.slug]);
    });
  }

  getProjectTypes() {
    console.log('Here we will take all the project types from backend');
    this.allProjectTypes = [
      'Startup',
      'Business',
      'Another'
    ];
    this.project.type = this.allProjectTypes[0];
  }

  deleteContributor() {
    if (this.params.contributorId && this.params.projectId) {
      this.deleteContributorFromBackend();
    }
  }

  deleteContributorFromBackend() {
    this.projectService.deleteContributor(this.params.contributorId, this.params.projectId).subscribe(project => {
      this.project = ProjectModel.mapData(project);
    }, error1 => {
      this.toastr.error(error1.error.message, 'Error');
    }, () => {
      this.toastr.success('You deleted contributor from your project', 'Success');
      this.actionsModal();
      this.router.navigate(['/app/project/' + this.project.slug]);
      this.project = null;
    });
  }

  getContributor() {
    this.contributorService.getContributor(this.params.contributorId).subscribe(data => {
      this.contributor = data;
    });
  }

  private getContributorStatuses() {
    this.allContributorStatuses = [
      {
        name: 'Active',
        data: 'active'
      },
      {
        name: 'Inactive',
        data: 'inactive'
      }
    ];
  }

  private updateContributor() {
    this.contributorService.update(this.contributor, this.contributor.id).subscribe(data => {
      this.contributor = ProjectContributorModel.mapData(data);
    }, error1 => {
      this.toastr.error(error1.error.message, 'Error');
    }, () => {
      this.toastr.success(`Contributor was updated!`, 'Success');
      this.actionsModal();
    });
  }
}
