import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-landing-partners',
  templateUrl: './landing-partners.component.html',
  styleUrls: ['./landing-partners.component.scss']
})
export class LandingPartnersComponent implements OnInit {
  allPartners;

  constructor() {
  }

  ngOnInit() {
    this.initPartners();
  }

  initPartners() {
    this.allPartners = [
      {
        name: 'yep',
        link: 'https://fb.com/yepmoldova'
      },
      {
        name: 'sens-media',
        link: 'https://sens.media'
      },
      {
        name: 'utm',
        link: 'https://utm.md'
      }
    ];
  }
}
