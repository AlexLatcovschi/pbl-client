import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LandingIntroComponent} from './landing-intro/landing-intro.component';
import {RouterModule} from '@angular/router';
import {LandingRegisterNowComponent} from './landing-register-now/landing-register-now.component';
import {LandingFeaturesComponent} from './landing-features/landing-features.component';
import {LandingVideoBlockComponent} from './landing-video-block/landing-video-block.component';
import {LandingPartnersComponent} from './landing-partners/landing-partners.component';
import {FeatureBoxComponent} from './feature-box/feature-box.component';
import {SlickCarouselModule} from 'ngx-slick-carousel';
import {FooterModule} from '../footer/footer.module';
import {LandingPageComponent} from '../../../app-layout/landing-page/landing-page.component';
import {NavbarModule} from '../navbar/navbar.module';
import {GradientButtonModule} from './gradient-button/gradient-button.module';


@NgModule({
  declarations: [
    LandingIntroComponent,
    LandingRegisterNowComponent,
    LandingFeaturesComponent,
    LandingVideoBlockComponent,
    LandingPartnersComponent,
    FeatureBoxComponent,
    LandingPageComponent,
  ],
  exports: [
    LandingIntroComponent,
    LandingRegisterNowComponent,
    LandingFeaturesComponent,
    LandingVideoBlockComponent,
    LandingPartnersComponent,
    LandingPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SlickCarouselModule,
    FooterModule,
    NavbarModule,
    GradientButtonModule
  ]
})
export class LandingModule {
}
