import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-landing-video-block',
  templateUrl: './landing-video-block.component.html',
  styleUrls: ['./landing-video-block.component.scss']
})
export class LandingVideoBlockComponent implements OnInit {
  @Input() isLogged;
  title;
  description;

  constructor() {
  }

  ngOnInit() {
    this.initData();
  }

  initData() {
    this.title = 'Everything starts from  an idea';
    this.description = 'How it works?';
  }

}
