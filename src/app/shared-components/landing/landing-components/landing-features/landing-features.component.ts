import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-landing-features',
  templateUrl: './landing-features.component.html',
  styleUrls: ['./landing-features.component.scss']
})
export class LandingFeaturesComponent implements OnInit {
  title;
  description;
  allFeatures;
  slideConfig;

  constructor() {
    this.slideConfig = {
      slidesToShow: 2,
      slidesToScroll: 1,
      arrows: false,
      autoplay: true,
      autoplaySpeed: 2000,
    };
  }

  ngOnInit() {
    this.initData();
    this.initFeatures();
  }

  initData() {
    this.title = 'Our features for you';
    this.description = 'Checkout features';
  }

  initFeatures() {
    this.allFeatures = [
      {
        image: 'assets/images/feature-image.png',
        title: 'One month free trial',
        description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer '
      },
    ];
    for (let i = 0; i < 10; i++) {
      this.allFeatures.push(this.allFeatures[0]);
    }
  }


}
