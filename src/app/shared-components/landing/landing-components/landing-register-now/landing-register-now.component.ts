import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing-register-now',
  templateUrl: './landing-register-now.component.html',
  styleUrls: ['./landing-register-now.component.scss']
})
export class LandingRegisterNowComponent implements OnInit {
  title;
  description;
  constructor() { }

  ngOnInit() {
    this.initData();
  }
  initData(){
    this.title = 'Register Today';
    this.description = 'and get started with a free 1 month trial for your Startup';
  }
}
