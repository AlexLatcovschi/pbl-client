import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-landing-intro',
  templateUrl: './landing-intro.component.html',
  styleUrls: ['./landing-intro.component.scss']
})
export class LandingIntroComponent implements OnInit {
  @Input() isLogged;
  title;
  description;

  constructor() {
  }

  ngOnInit() {
    this.initData();
  }

  initData() {
    this.title = 'Prefect solution for startups';
    this.description = 'Get right now a free demo and start building your startups more efficiently';
  }

}
