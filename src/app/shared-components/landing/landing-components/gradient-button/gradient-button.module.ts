import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {GradientButtonComponent} from './gradient-button.component';

@NgModule({
  declarations: [GradientButtonComponent],
  exports: [GradientButtonComponent],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class GradientButtonModule {
}
