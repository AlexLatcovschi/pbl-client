import {Component, Input, OnInit} from '@angular/core';
import {AuthService} from '../../../core/auth/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @Input() hasBackground;
  @Input() isLogged;
  links;

  constructor(
    private authService: AuthService,
  ) {
  }

  ngOnInit() {
    this.initLinks();
  }

  initLinks() {
    this.links = [
      {
        name: 'Home',
        link: '/'
      },
      {
        name: 'Features',
        link: '#features'
      },
      {
        name: 'Partners',
        link: '#partners'
      },
      {
        name: 'Login',
        link: '/account/login'
      },
      {
        name: 'Logout',
        link: ''
      }
    ];
  }

  logout() {
    this.authService.logout(true);
  }
}
