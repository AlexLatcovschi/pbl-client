import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NavbarComponent} from './navbar.component';
import {GradientButtonModule} from '../landing-components/gradient-button/gradient-button.module';

@NgModule({
  imports: [CommonModule, RouterModule, GradientButtonModule],
  declarations: [
    NavbarComponent,
  ],
  exports: [
    NavbarComponent
  ],
})
export class NavbarModule {
}
