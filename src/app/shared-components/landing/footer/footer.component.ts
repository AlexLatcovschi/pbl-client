import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  links;

  constructor() {
  }

  ngOnInit() {
    this.initLinks();
  }

  initLinks() {
    this.links = [
      {
        name: 'mail',
        link: 'mailto: info@gmail.com'
      },
      {
        name: 'phone',
        link: 'tel: +37368645304'
      },
      {
        name: 'messenger',
        link: 'm.me/test'
      }
    ];
  }

}
