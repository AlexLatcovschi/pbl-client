import {Component, OnInit} from '@angular/core';
import {NotificationModel} from '../../../models/notification.model';
import {NotificationService} from '../../../services/notification.service';
import {ProjectService} from '../../../services/project.service';
import * as moment from 'moment';

@Component({
  selector: 'app-notification-block',
  templateUrl: './notification-block.component.html',
  styleUrls: ['./notification-block.component.scss']
})
export class NotificationBlockComponent implements OnInit {

  public notifications: NotificationModel[];
  public notificationCount = 0;
  public showTooltip = false;

  constructor(public notificationService: NotificationService,
              public projectService: ProjectService) {
  }

  async ngOnInit() {
    await this.getNotifications();
  }

  public async getNotifications() {
    await this.notificationService.query().subscribe(res => {
      if (res) {
        this.notifications = res.body.map(el => {
          el.createdAt = moment(el.createdAt).format('DD.MM.YY');
          if (el.projectId) {
            this.projectService.getProjectById(el.projectId).subscribe(project => {
              if (project) {
                el.fullProject = project[0];
              }
            });
          }
          return el;
        });
        this.notifications = this.notifications.slice(0, 7);
        this.countNotifications();
      }
    });
  }

  public async setNotificationSeen(notification: NotificationModel) {
    notification.seen = true;
    this.notificationCount--;
    console.log(notification);
    if (notification.id) {
      await this.notificationService.update(notification, notification.id).subscribe();
    }
  }

  public markAllAsRead() {
    this.notifications.map(async el => {
      el.seen = true;
      this.notificationCount--;
      if (el.id) {
        await this.notificationService.update(el, el.id).subscribe();
      }
    });
  }

  public countNotifications() {
    console.log(this.notifications);
    this.notifications.map(el => {
      if (!el.seen) {
        this.notificationCount++;
      }
    });
  }
}
