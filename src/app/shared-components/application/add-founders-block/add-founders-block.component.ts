import {Component, Input, OnInit} from '@angular/core';
import {UserModel} from '../../../models/user.model';
import {ProjectService} from '../../../services/project.service';
import {ToastrService} from 'ngx-toastr';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';

@Component({
  selector: 'app-add-founders-block',
  templateUrl: './add-founders-block.component.html',
  styleUrls: ['./add-founders-block.component.scss']
})
export class AddFoundersBlockComponent implements OnInit {
  @Input() projectId;
  openInput = false;
  users: UserModel[] = [];
  debounceTime = 1000;
  queryUsers: string;
  params;
  userPage = 0;
  userSize = 4;
  searchTermChanged: Subject<string> = new Subject<string>();
  showLoadMoreUserBtn;
  showList;

  constructor(
    private projectService: ProjectService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit() {
    this.getUsersWithDebounceTime();

  }

  clickInput() {
    this.openInput = !this.openInput;
    this.showList = false;
  }

  addUser(user: UserModel) {
    this.sendUserToBackend(user);
  }

  sendUserToBackend(user: UserModel) {
    this.projectService.addFounder(user.id, this.projectId).subscribe(() => {
    }, error => {
      this.toastr.error(error.error.message, 'Error');
    }, () => {
      this.toastr.success(`You added ${user.firstName} ${user.lastName} to your project`, 'Success');
    });
  }

  changed(text: string) {
    this.showList = true;
    this.searchTermChanged.next(text);
  }

  getUsersWithDebounceTime() {
    this.searchTermChanged
      .pipe(
        debounceTime(this.debounceTime),
        distinctUntilChanged(),
      )
      .subscribe(userName => {
        this.users = [];
        if (userName.length >= 2) {
          this.params = {
            projectId: this.projectId,
            name: userName,
            page: this.userPage,
            size: this.userSize,
          };
          this.getUsersFromBackend();
        }
      });
  }

  getUsersFromBackend() {
    this.projectService.getUsers(this.params).subscribe(({body, headers}) => {
      const response = body;

      const users = UserModel.mapMultipleUser(response);
      const totalItems = headers.get('X-Total-Count');

      if (response && response.length > 0) {
        this.users = [...this.users, ...users];

        this.showLoadMoreUserBtn = !(Number(totalItems) === this.users.length);
      } else {
        this.showLoadMoreUserBtn = false;
      }
    });
  }

  loadMoreUsers() {
    this.params.page++;
    this.getUsersFromBackend();
  }
}
