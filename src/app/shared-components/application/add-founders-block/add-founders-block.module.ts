import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {AddFoundersBlockComponent} from './add-founders-block.component';
import {FormsModule} from '@angular/forms';
import {AppAvatarModule} from '../app-avatar/app-avatar.module';
import {ViewUserNameModule} from '../../general/view-user-name/view-user-name.module';

@NgModule({
  imports: [CommonModule, RouterModule, FormsModule, AppAvatarModule, ViewUserNameModule],
  declarations: [
    AddFoundersBlockComponent,
  ],
  exports: [
    AddFoundersBlockComponent
  ],
})
export class AddFoundersBlockModule {
}
