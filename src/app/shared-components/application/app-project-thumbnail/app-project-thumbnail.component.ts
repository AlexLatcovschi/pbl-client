import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserModel} from '../../../models/user.model';
import {environment} from '../../../../environments/environment';
import {ProjectModel} from '../../../models/project';
import {IMAGE_PREFIX} from '../../../app.constants';

@Component({
  selector: 'app-app-project-thumbnail',
  templateUrl: './app-project-thumbnail.component.html',
  styleUrls: ['./app-project-thumbnail.component.scss']
})
export class AppProjectThumbnailComponent implements OnInit {
  @Output() public fileChange = new EventEmitter<File>();
  @Input() project: ProjectModel;
  @Input() avatarSize: number;
  @Input() editImage: boolean;

  accept = 'image/png, image/jpeg, image/jpg';
  addPrefix: boolean;
  prefix = IMAGE_PREFIX;

  constructor() {
  }

  ngOnInit() {
  }

  handleFileInput(file: FileList) {
    this.fileChange.emit(file.item(0));
    this.addPrefix = true;
  }
}
