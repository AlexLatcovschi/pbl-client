import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {AppAvatarComponent} from './app-avatar.component';
import {PipeModule} from '../../../pipe/pipe.module';

@NgModule({
  imports: [CommonModule, RouterModule, PipeModule],
  declarations: [
    AppAvatarComponent,
  ],
  exports: [
    AppAvatarComponent
  ],
})
export class AppAvatarModule {
}
