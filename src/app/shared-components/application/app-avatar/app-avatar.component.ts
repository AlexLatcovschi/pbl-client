import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserModel} from '../../../models/user.model';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-app-avatar',
  templateUrl: './app-avatar.component.html',
  styleUrls: ['./app-avatar.component.scss']
})
export class AppAvatarComponent implements OnInit {
  @Output() public fileChange = new EventEmitter<File>();
  @Input() user: UserModel;
  @Input() avatarSize = 40;
  @Input() editImage: boolean;

  accept = 'image/png, image/jpeg, image/jpg';
  prefix = environment.serverUrl + `/api/`;

  constructor() {
  }

  ngOnInit() {
  }

  handleFileInput(file: FileList) {
    this.fileChange.emit(file.item(0));
  }
}
