import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {AppHeaderComponent} from './app-header.component';
import {AppProfileBlockModule} from '../app-profile-block/app-profile-block.module';
import {NotificationBlockComponent} from '../notification-block/notification-block.component';
import {BlocksModule} from '../blocks/blocks.module';
import {PipeModule} from '../../../pipe/pipe.module';

@NgModule({
  imports: [CommonModule, RouterModule, AppProfileBlockModule, BlocksModule, PipeModule],
  declarations: [
    AppHeaderComponent,
    NotificationBlockComponent
  ],
  exports: [
    AppHeaderComponent
  ],
})
export class AppHeaderModule {
}
