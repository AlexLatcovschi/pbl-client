import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserModel} from '../../../models/user.model';
import {AuthService} from '../../../core/auth/auth.service';

@Component({
  selector: 'app-app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss']
})
export class AppHeaderComponent implements OnInit {
  @Input() user: UserModel;
  @Output() openModal = new EventEmitter();
  allLinks: any;

  constructor(
    private authService: AuthService
  ) {
  }

  ngOnInit() {
    this.initLinks();
  }

  openCreateProjectModal() {
    this.openModal.emit(true);
  }

  initLinks() {
    this.allLinks = [
      {
        name: 'Home',
        icon: 'assets/icons/home-header.svg',
        link: '/'
      },
      {
        name: 'Logout',
        icon: 'assets/icons/logout-header.svg',
        action: 'logout'
      }
    ];
  }

  action(action) {
    if (action === 'logout') {
      this.authService.logout(true);
    }
  }
}
