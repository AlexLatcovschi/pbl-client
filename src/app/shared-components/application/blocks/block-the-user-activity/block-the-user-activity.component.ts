import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {ProjectContributorModel} from '../../../../models/project-contributor.model';
import {ContributionModel} from '../../../../models/contribution.model';
import {ProjectContributorService} from '../../../../services/project-contributor.service';

@Component({
  selector: 'app-block-the-user-activity',
  templateUrl: './block-the-user-activity.component.html',
  styleUrls: ['./block-the-user-activity.component.scss']
})
export class BlockTheUserActivityComponent implements OnChanges {
  @Input() contributor: ProjectContributorModel;
  allContributions: ContributionModel[];

  constructor(
    private service: ProjectContributorService
  ) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes && this.contributor) {
      this.getContributions();
    }
  }

  getContributions() {
    this.service.getContributions(this.contributor.id).subscribe(data => {
      this.allContributions = data;
    });
  }

}
