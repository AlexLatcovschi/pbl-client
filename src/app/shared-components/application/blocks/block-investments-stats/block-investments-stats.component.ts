import {Component, OnInit} from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-block-investments-stats',
  templateUrl: './block-investments-stats.component.html',
  styleUrls: ['./block-investments-stats.component.scss']
})
export class BlockInvestmentsStatsComponent implements OnInit {

  constructor() {
  }

  public barChartLabels = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
  public barChartType = 'bar';
  public barChartLegend = false;
  public showDatePicker = false;
  public currentDateRange = 'This week';
  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Snacks',
          beginAtZero: true

        }
      }],
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Weekdays'
        }
      }]
    },
    tooltips: {
      enabled: true,
      mode: 'single',
      custom: (tooltip) => {
        if (!tooltip) {
          return;
        }
        tooltip.displayColors = false;
      },
      callbacks: {
        label: (tooltipItems, data) => {
          const multistringText = ['Here will be parsed date!'];
          multistringText.push(' ');
          multistringText.push(tooltipItems.yLabel);
          multistringText.push('Another Item');
          multistringText.push(tooltipItems.index + 1);
          multistringText.push('One more Item');
          return multistringText;
        },
        labelTextColor: () => {
          return '#543453';
        },
      },
      backgroundColor: 'white',
      cornerRadius: 9,
      caretSize: 0,
      boxShadow: '0px 4px 15px rgba(0, 0, 0, 0.25)'
    },
  };
  public barChartData = [
    {
      data: [65, 59, 80, 81, 56, 55, 45],
      label: 'Series A'
    }
  ];
  public chartColor: Array<any> = [
    {
      backgroundColor: () => {
        return '#DA5834';
      },
      borderWidth: 0,
    }
  ];
  public ranges: any = {
    'This week': [moment().clone().startOf('isoWeek')],
    'Last week': [moment().subtract(1, 'week').startOf('isoWeek'), moment().subtract(1, 'week').endOf('isoWeek')],
    'This Month': [moment().startOf('month'), moment().endOf('month')],
    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
  };
  public dateOptions = {
    firstDay: 1
  };

  ngOnInit() {
  }

  chooseDate(event) {
    this.showDatePicker = false;
    console.log('my event: ', event);
  }

  rangeClicked(event) {
    this.currentDateRange = event.label;
    console.log('event from range click: ', event);
  }

}
