import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {ProjectContributorModel} from '../../../../models/project-contributor.model';

@Component({
  selector: 'app-block-the-team',
  templateUrl: './block-the-team.component.html',
  styleUrls: ['./block-the-team.component.scss']
})
export class BlockTheTeamComponent implements OnChanges {
  @Input() allContributors: ProjectContributorModel[];
  @Input() projectSlug: string;
  public activeMenu = 'active';
  public contributors: ProjectContributorModel[];
  inactiveUsers: boolean;

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.allContributors && this.allContributors) {
      this.filterTeam(this.activeMenu);
      this.checkIfInactiveUsersExists();
    }
  }

  filterTeam(key: string) {
    this.activeMenu = key;
    this.contributors = this.allContributors.filter((contributor) => contributor.status === key);
  }

  checkIfInactiveUsersExists() {
    const inactiveContributors = this.allContributors.filter((contributor) => contributor.status === 'inactive');
    this.inactiveUsers = inactiveContributors.length > 0;
  }

}
