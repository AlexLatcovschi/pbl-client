import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-block-projects-stats',
  templateUrl: './block-projects-stats.component.html',
  styleUrls: ['./block-projects-stats.component.scss']
})
export class BlockProjectsStatsComponent implements OnInit {
  public projectsCount = 3;
  public contributor = 'Director';
  public contributorProjects = 2;
  public lastActivity = 'octomber 12, 2019';
  public averageOwns = '68%';
  public snacks = '3412.00';

  constructor() {
  }

  ngOnInit() {
  }

}
