import {Component, Input, OnInit} from '@angular/core';
import {HttpEventType} from '@angular/common/http';
import {ProjectService} from '../../../../services/project.service';
import {FileService} from '../../../../services/file.service';
import {ProjectModel} from '../../../../models/project';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';

@Component({
  selector: 'app-block-edit-project',
  templateUrl: './block-edit-project.component.html',
  styleUrls: ['./block-edit-project.component.scss']
})
export class BlockEditProjectComponent implements OnInit {
  @Input() projectSlug;
  showActions;
  project: ProjectModel;
  projectSettings;
  tempProfileImage;

  constructor(private projectService: ProjectService,
              private fileService: FileService,
              private toastr: ToastrService,
              private router: Router) {
  }

  ngOnInit() {
    this.initProject();
  }


  initProject() {
    this.projectService.getProjectBySlug(this.projectSlug).subscribe(data => {
      this.project = data[0];
      this.initProjectSettings();
    });
  }


  initProjectSettings() {
    this.projectSettings = [
      {
        id: 0,
        title: 'Name',
        value: this.project.name,
        slug: 'name',
        showEdit: true,
        previousValue: this.project.name
      },
      {
        id: 1,
        title: 'Slug',
        value: this.project.slug,
        slug: 'slug',
        showEdit: true,
        previousValue: this.project.slug

      },
      {
        id: 2,
        title: 'Type',
        value: this.project.type,
        slug: 'type',
        showEdit: true,
        previousValue: this.project.type
      }
    ];
  }

  editItem(item) {
    this.projectSettings[item].showEdit = false;
    this.projectSettings[item].previousValue = this.projectSettings[item].value;
  }

  saveData() {
    this.resetFields();
    this.updateProjectData();
    this.sendDataToBackend();
    this.showActions = false;
    this.tempProfileImage = this.project.imageUrl;
  }

  cancelData() {
    this.resetFields();
    this.showActions = false;
    this.fileService.deleteProjectFile(this.project.imageUrl).subscribe();
    this.project.imageUrl = this.tempProfileImage;
  }

  resetFields() {
    this.projectSettings.map(data => {
      return data.showEdit = true;
    });
  }

  updateProjectData() {
    this.project.name = this.projectSettings[0].value;
    this.project.slug = this.projectSettings[1].value;
    this.project.type = this.projectSettings[2].value;
  }

  sendDataToBackend() {
    this.projectService.update(this.project, this.project.id).subscribe(data => {
      this.project.slug = data.slug;
      }, error => {
        this.toastr.error(error.error.message, 'Error');
      },
      () => {
        this.toastr.success('You have been updated project successfully!', 'Success');
        this.router.navigate(['/app/project/' + this.project.slug + '/edit'], {
          queryParams: { refresh: new Date().getTime() },
        });
      });
  }

  saveItem(item) {
    this.showActions = true;
    this.projectSettings[item].showEdit = true;
  }

  cancelItem(item) {
    this.projectSettings[item].showEdit = true;
    this.projectSettings[item].value = this.projectSettings[item].previousValue;
  }

  changeProjectImage(image) {
    this.projectService.uploadImage(image).subscribe(
      (events: any) => {
        if (events.type === HttpEventType.UploadProgress) {
          console.log('Upload progress: ', Math.round((events.loaded / events.total) * 100) + '%');
        } else if (events.type === HttpEventType.Response) {
          this.project.imageUrl = events.body.path;
        }
      }
    );
  }

  handleProfileImage(image) {
    this.showActions = true;
    this.changeProjectImage(image);
  }
}
