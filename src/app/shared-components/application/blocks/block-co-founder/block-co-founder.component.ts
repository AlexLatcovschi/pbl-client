import {Component, Input} from '@angular/core';
import {ProjectContributorModel} from '../../../../models/project-contributor.model';

@Component({
  selector: 'app-block-co-founder',
  templateUrl: './block-co-founder.component.html',
  styleUrls: ['./block-co-founder.component.scss']
})
export class BlockCoFounderComponent {
  @Input() contributor: ProjectContributorModel;

  constructor(
  ) {
  }


}
