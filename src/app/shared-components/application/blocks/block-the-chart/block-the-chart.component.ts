import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {ProjectContributorModel} from '../../../../models/project-contributor.model';

@Component({
  selector: 'app-block-the-chart',
  templateUrl: './block-the-chart.component.html',
  styleUrls: ['./block-the-chart.component.scss']
})
export class BlockTheChartComponent implements OnChanges {
  @Input() allContributors: ProjectContributorModel[];
  public activeChart: any = 'pie';
  public contributors = [];
  public data = [];
  public chartColor: any = [
    {
      backgroundColor: () => {
        return [];
      },
      borderWidth: 0,
    }
  ];
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    tooltips: {
      enabled: true,
      mode: 'single',
      custom: (tooltip) => {
        if (!tooltip) {
          return;
        }
        tooltip.displayColors = false;
      },
      callbacks: {
        label: (tooltipItems, data) => {
          const multistringText =
            [this.allContributors[tooltipItems.index].user.firstName + ' ' + this.allContributors[tooltipItems.index].user.lastName];
          multistringText.push(' ');
          multistringText.push('Snacks: ' + this.allContributors[tooltipItems.index].snacks);
          multistringText.push('Actions: ' + this.allContributors[tooltipItems.index].actions + ' %');
          return multistringText;
        },
        labelTextColor: () => {
          return '#543453';
        }
      },
      backgroundColor: 'white',
      cornerRadius: 9,
      caretSize: 0,
      boxShadow: '0px 4px 15px rgba(0, 0, 0, 0.25)'
    },
    legend: {
      display: false
    },
    scales: {
      yAxes: [{
        display: false
      }],
      xAxes: [{
        display: false,
        barPercentage: 1.0,

      }],
    },
  };

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.allContributors && this.allContributors) {
      this.initChart();
    }
  }

  initChart() {
    const colors = [];
    this.allContributors.map(contributor => {
      this.contributors.push(contributor.user.firstName + ' ' + contributor.user.lastName);
      colors.push(contributor.color);
      this.data.push(contributor.actions);
    });
    this.chartColor[0] = {
      backgroundColor: () => {
        return colors;
      },
      borderWidth: 0,
    };
  }

  changeChart(key: string) {
    this.activeChart = key;
  }
}
