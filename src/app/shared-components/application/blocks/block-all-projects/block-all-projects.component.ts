import {Component, Input, OnInit} from '@angular/core';
import {ProjectModel} from '../../../../models/project';
import {ProjectService} from '../../../../services/project.service';

@Component({
  selector: 'app-block-all-projects',
  templateUrl: './block-all-projects.component.html',
  styleUrls: ['./block-all-projects.component.scss']
})
export class BlockAllProjectsComponent implements OnInit {
  @Input() project: ProjectModel;
  public slideConfig;

  constructor() {
    this.slideConfig = {
      arrows: false,
    };
  }

  ngOnInit() {
  }

}
