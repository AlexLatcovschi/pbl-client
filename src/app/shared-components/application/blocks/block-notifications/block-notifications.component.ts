import {Component, OnInit} from '@angular/core';
import {NotificationService} from '../../../../services/notification.service';
import {NotificationModel} from '../../../../models/notification.model';
import {UserDataService} from '../../../../core/auth/user-data.service';
import * as moment from 'moment';
import {ProjectService} from '../../../../services/project.service';

@Component({
  selector: 'app-block-notifications',
  templateUrl: './block-notifications.component.html',
  styleUrls: ['./block-notifications.component.scss']
})
export class BlockNotificationsComponent implements OnInit {
  public notifications: NotificationModel[];

  constructor(public notificationService: NotificationService,
              public userService: UserDataService,
              public projectService: ProjectService) {
  }

  async ngOnInit() {
    await this.getNotifications();
  }

  public async getNotifications() {
    await this.notificationService.query().subscribe(res => {
      if (res) {
        this.notifications = res.body.map(el => {
          el.createdAt = moment(el.createdAt).format('DD.MM.YY');
          if (el.projectId) {
            this.projectService.getProjectById(el.projectId).subscribe(project => {
              if (project) {
                el.fullProject = project[0];
              }
            });
          }
          return el;
        });
      }
    });
  }

  public async setNotificationSeen(notification: NotificationModel) {
    notification.seen = true;
    await this.notificationService.update(notification, notification.id).subscribe();
  }

  public markAllAsRead() {
    this.notifications.map(async el => {
      el.seen = true;
      await this.notificationService.update(el, el.id).subscribe();
    });
  }
}
