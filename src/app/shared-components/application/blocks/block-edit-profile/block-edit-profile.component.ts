import {Component, OnInit} from '@angular/core';
import {UserModel} from '../../../../models/user.model';
import {UserDataService} from '../../../../core/auth/user-data.service';
import {ToastrService} from 'ngx-toastr';
import {AccountService} from '../../../../core/auth/account.service';
import {HttpEventType} from '@angular/common/http';
import {FileService} from '../../../../services/file.service';

@Component({
  selector: 'app-block-edit-profile',
  templateUrl: './block-edit-profile.component.html',
  styleUrls: ['./block-edit-profile.component.scss']
})
export class BlockEditProfileComponent implements OnInit {
  user: UserModel;
  userSettings;
  showActions;
  showModal;
  tempProfileImage;

  constructor(
    private userService: UserDataService,
    private toastr: ToastrService,
    private accountService: AccountService,
    private fileService: FileService
  ) {
  }

  ngOnInit() {
    this.getProfile();
  }

  getProfile() {
    this.userService.getUser().subscribe(data => {
      this.user = data;
      if (this.user) {
        this.initUserSettings();
      }
    });
  }

  initUserSettings() {
    this.userSettings = [
      {
        id: 0,
        title: 'First Name',
        value: this.user.firstName,
        slug: 'fistName',
        showEdit: true,
        previousValue: this.user.firstName
      },
      {
        id: 1,
        title: 'Last Name',
        value: this.user.lastName,
        slug: 'lastName',
        showEdit: true,
        previousValue: this.user.lastName

      },
      {
        id: 2,
        title: 'Bio',
        value: this.user.description,
        slug: 'description',
        showEdit: true,
        previousValue: this.user.description
      },
      {
        id: 3,
        title: 'Email',
        value: this.user.email,
        slug: 'email',
        showEdit: true,
        previousValue: this.user.email
      },
      {
        id: 4,
        title: 'Password',
        value: '********',
        slug: 'password',
        showEdit: true,
        previousValue: this.user.password
      }
    ];
    this.tempProfileImage = this.user.pictureUrl;
  }

  editItem(item) {
    if (item !== 4) {
      this.userSettings[item].showEdit = false;
      this.userSettings[item].previousValue = this.userSettings[item].value;
    } else {
      this.showModal = true;
    }

  }

  saveItem(item) {
    this.showActions = true;
    this.userSettings[item].showEdit = true;
  }

  cancelItem(item) {
    this.userSettings[item].showEdit = true;
    this.userSettings[item].value = this.userSettings[item].previousValue;
  }

  saveData() {
    this.resetFields();
    this.updateUserData();
    this.sendDataToBackend();
    this.showActions = false;
    this.tempProfileImage = this.user.pictureUrl;
  }

  cancelData() {
    this.resetFields();
    this.showActions = false;
    this.fileService.deleteFile(this.user.pictureUrl).subscribe();
    this.user.pictureUrl = this.tempProfileImage;
    this.toastr.info('Your profile settings has not been changed!', 'Cancel!');
  }

  sendDataToBackend() {
    this.accountService.updateUserData(this.user).subscribe(
      () => {
      },
      err => {
      },
      () => {
        this.toastr.success('Your profile settings has been changed successfully!', 'Save!');
      },
    );
  }

  resetFields() {
    this.userSettings.map(data => {
      return data.showEdit = true;
    });
  }

  updateUserData() {
    this.user.firstName = this.userSettings[0].value;
    this.user.lastName = this.userSettings[1].value;
    this.user.description = this.userSettings[2].value;
    this.user.email = this.userSettings[3].value;
  }

  updateModal(event) {
    this.showModal = event;
  }

  changeProfileImage(image) {
    this.accountService.uploadImage(image).subscribe(
      (events: any) => {
        if (events.type === HttpEventType.UploadProgress) {
          console.log('Upload progress: ', Math.round((events.loaded / events.total) * 100) + '%');
        } else if (events.type === HttpEventType.Response) {
          this.user.pictureUrl = events.body.path;
        }
      },
      () => {
        this.toastr.error('Image is too big', 'Error');
      },
    );
  }

  handleProfileImage(image) {
    /*
        TO DO !!!
        Add current uploaded image to the user, so only him could delete this image from server
    */
    this.showActions = true;
    this.changeProfileImage(image);
  }
}
