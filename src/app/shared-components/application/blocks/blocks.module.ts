import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {BlockInvestmentsStatsComponent} from './block-investments-stats/block-investments-stats.component';
import {BlockProjectsStatsComponent} from './block-projects-stats/block-projects-stats.component';
import {BlockAllProjectsComponent} from './block-all-projects/block-all-projects.component';
import {BlockYourProjectComponent} from './block-your-project/block-your-project.component';
import {BlockTheTeamComponent} from './block-the-team/block-the-team.component';
import {BlockTheChartComponent} from './block-the-chart/block-the-chart.component';
import {BlockCoFounderComponent} from './block-co-founder/block-co-founder.component';
import {BlockTheUserActivityComponent} from './block-the-user-activity/block-the-user-activity.component';
import {BlockNotificationsComponent} from './block-notifications/block-notifications.component';
import {BlockEditProfileComponent} from './block-edit-profile/block-edit-profile.component';
import {BlockEditProjectComponent} from './block-edit-project/block-edit-project.component';
import {AppAvatarModule} from '../app-avatar/app-avatar.module';
import {FormsModule} from '@angular/forms';
import {ToastrModule} from 'ngx-toastr';
import {ChartsModule} from 'ng2-charts';
import {NgxDaterangepickerMd} from 'ngx-daterangepicker-material';
import {SlickCarouselModule} from 'ngx-slick-carousel';
import {AppProjectThumbnailComponent} from '../app-project-thumbnail/app-project-thumbnail.component';
import {PipeModule} from '../../../pipe/pipe.module';
import {ModalModule} from '../../general/modal/modal.module';
import {ViewUserNameModule} from '../../general/view-user-name/view-user-name.module';
import {AddFoundersBlockModule} from '../add-founders-block/add-founders-block.module';
import {BulletBtnModule} from '../bullet-btn/bullet-btn.module';

@NgModule({
  imports: [CommonModule, RouterModule, AppAvatarModule, FormsModule, ToastrModule,
    ChartsModule, NgxDaterangepickerMd.forRoot(), ModalModule, SlickCarouselModule,
    PipeModule, ViewUserNameModule, AddFoundersBlockModule, BulletBtnModule],
  declarations: [
    BlockInvestmentsStatsComponent,
    BlockProjectsStatsComponent,
    BlockAllProjectsComponent,
    BlockYourProjectComponent,
    BlockTheTeamComponent,
    BlockTheChartComponent,
    BlockCoFounderComponent,
    BlockTheUserActivityComponent,
    BlockNotificationsComponent,
    BlockEditProfileComponent,
    BlockEditProjectComponent,
    AppProjectThumbnailComponent
  ],
  exports: [
    BlockInvestmentsStatsComponent,
    BlockProjectsStatsComponent,
    BlockAllProjectsComponent,
    BlockEditProfileComponent,
    BlockYourProjectComponent,
    BlockTheTeamComponent,
    BlockTheChartComponent,
    BlockCoFounderComponent,
    BlockTheUserActivityComponent,
    BlockEditProjectComponent,
    BlockNotificationsComponent
  ],
})
export class BlocksModule {
}
