import {Component, Input, OnInit} from '@angular/core';
import {ProjectModel} from '../../../../models/project';
import {ShareService} from '../../../../services/share.service';

@Component({
  selector: 'app-block-your-project',
  templateUrl: './block-your-project.component.html',
  styleUrls: ['./block-your-project.component.scss']
})
export class BlockYourProjectComponent implements OnInit {
  @Input() project: ProjectModel;

  constructor(
    private sharedService: ShareService
  ) {
  }

  ngOnInit() {
  }

  addContribution() {
    const params = {
      projectId: this.project.id,
      enabled: true
    };
    this.sharedService.sendAddContribution(params);
  }
}
