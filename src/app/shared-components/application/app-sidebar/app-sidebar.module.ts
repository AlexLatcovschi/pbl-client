import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {AppSidebarComponent} from './app-sidebar.component';

@NgModule({
  imports: [CommonModule, RouterModule],
  declarations: [
    AppSidebarComponent,
  ],
  exports: [
    AppSidebarComponent
  ],
})
export class AppSidebarModule {
}
