import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-app-sidebar',
  templateUrl: './app-sidebar.component.html',
  styleUrls: ['./app-sidebar.component.scss']
})
export class AppSidebarComponent implements OnInit {
  allItems;

  constructor() {
  }

  ngOnInit() {
    this.initItems();
  }

  initItems() {
    this.allItems = [
      {
        title: 'General',
        menus: [
          {
            title: 'Home',
            link: '/app',
            iconInactive: 'menu-home-inactive',
            iconActive: 'menu-home-active'
          },
          {
            title: 'Projects',
            link: '/app/projects',
            iconInactive: 'menu-projects-inactive',
            iconActive: 'menu-projects-active'
          },
          {
            title: 'My Profile',
            link: '/app/profile',
            iconInactive: 'menu-user-inactive',
            iconActive: 'menu-user-active'
          },
        ],
      },
    ];
  }
}
