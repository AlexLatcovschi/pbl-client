import {Component, Input, OnInit} from '@angular/core';
import {UserModel} from '../../../models/user.model';

@Component({
  selector: 'app-app-profile-block',
  templateUrl: './app-profile-block.component.html',
  styleUrls: ['./app-profile-block.component.scss']
})
export class AppProfileBlockComponent implements OnInit {
  @Input() user: UserModel;

  constructor() {
  }

  ngOnInit() {
  }

}
