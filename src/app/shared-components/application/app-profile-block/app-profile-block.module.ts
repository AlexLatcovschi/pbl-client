import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {AppProfileBlockComponent} from './app-profile-block.component';
import {AppAvatarModule} from '../app-avatar/app-avatar.module';
import {ViewUserNameModule} from '../../general/view-user-name/view-user-name.module';

@NgModule({
  imports: [CommonModule, RouterModule, AppAvatarModule, ViewUserNameModule],
  declarations: [
    AppProfileBlockComponent,
  ],
  exports: [
    AppProfileBlockComponent
  ],
})
export class AppProfileBlockModule {
}
