import {Component, Input, OnInit} from '@angular/core';
import {ShareService} from '../../../services/share.service';

@Component({
  selector: 'app-bullet-btn',
  templateUrl: './bullet-btn.component.html',
  styleUrls: ['./bullet-btn.component.scss']
})
export class BulletBtnComponent implements OnInit {
  @Input() contributorId: number;
  @Input() projectId: number;
  @Input() template;
  showEdit = false;
  actions;

  constructor(
    private sharedService: ShareService
  ) {
  }

  ngOnInit() {
    this.initActions();
  }

  initActions() {
    this.actions = [
      {
        name: 'Edit',
        slug: 'edit',
      },
      {
        name: 'Delete',
        slug: 'delete'
      }
    ];
  }

  clickBtn() {
    this.showEdit = !this.showEdit;
  }

  deleteUser() {
    const params = {
      contributorId: this.contributorId,
      projectId: this.projectId,
      enabled: true
    };
    this.sharedService.sendShowDeleteUserModal(params);
  }

  editUser() {
    const params = {
      contributorId: this.contributorId,
      enabled: true
    };
    this.sharedService.sendShowEditUserModal(params);
  }
}
