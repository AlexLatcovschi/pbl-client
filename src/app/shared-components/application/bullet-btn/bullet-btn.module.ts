import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {BulletBtnComponent} from './bullet-btn.component';

@NgModule({
  imports: [CommonModule, RouterModule],
  declarations: [
    BulletBtnComponent,
  ],
  exports: [
    BulletBtnComponent
  ],
})
export class BulletBtnModule {
}
