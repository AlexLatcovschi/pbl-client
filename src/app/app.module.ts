import {BrowserModule, BrowserTransferStateModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from './app-routing.module';
import {AppLayoutModule} from './app-layout/app-layout.module';
import {SlickCarouselModule} from 'ngx-slick-carousel';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NgxWebstorageModule} from 'ngx-webstorage';
import {CookieService} from 'ngx-cookie-service';
import {TransferHttpResponseInterceptor} from './core/interceptors/transfer-http-response-interceptor.service';
import {TokenInterceptor} from './core/interceptors/token.interceptor';
import {PipeModule} from './pipe/pipe.module';
import {ToastrModule} from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ChartsModule} from 'ng2-charts';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import {InvestmentService} from './services/investment.service';
import {JhiEventManager} from './services/event-manager.service';
import {Principal} from './services/principal.service';
import {AuthServerProvider} from './services/auth-jwt.service';
import {PushNotificationService} from './services/push-notifications.service';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserTransferStateModule,
    RouterModule,
    AppRoutingModule,
    AppLayoutModule,
    SlickCarouselModule,
    PipeModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(),
    NgxWebstorageModule.forRoot({
      prefix: '',
      separator: '',
      caseSensitive: true,
    }),
    ChartsModule,
    NgxDaterangepickerMd.forRoot(),
    ServiceWorkerModule.register('/sw.js', {enabled: environment.production})
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TransferHttpResponseInterceptor,
      multi: true,
    },
    CookieService,
    InvestmentService,
    PushNotificationService,
    JhiEventManager,
    Principal,
    AuthServerProvider
  ],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
