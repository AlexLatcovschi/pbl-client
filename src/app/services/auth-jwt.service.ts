import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {LocalStorageService} from 'ngx-webstorage';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {SERVER_API_URL} from '../app.constants';

@Injectable({providedIn: 'root'})
export class AuthServerProvider {
  constructor(private http: HttpClient, private $localStorage: LocalStorageService) {
  }

  getToken() {
    return this.$localStorage.retrieve('authenticationToken');
  }

  login(credentials: any): Observable<any> {
    const data = {
      email: credentials.email,
      password: credentials.password
    };
    return this.http.post(SERVER_API_URL + 'api/auth', data, {observe: 'response'}).pipe(
      map(({headers}) => {
        const bearerToken = headers.get('Authorization');
        if (bearerToken && bearerToken.slice(0, 7) === 'Bearer ') {
          const jwt = bearerToken.slice(7, bearerToken.length);
          this.storeAuthenticationToken(jwt);
          return jwt;
        }
        return undefined;
      })
    );
  }

  loginWithToken(jwt: string): Promise<string> {
    if (jwt) {
      this.storeAuthenticationToken(jwt);
      return Promise.resolve(jwt);
    } else {
      return Promise.reject('auth-jwt-service Promise reject'); // Put appropriate error message here
    }
  }

  storeAuthenticationToken(jwt: string) {
    this.$localStorage.store('authenticationToken', jwt);
  }

  logout(): Observable<any> {
    return new Observable(observer => {
      this.$localStorage.clear('authenticationToken');
      observer.complete();
    });
  }
}
