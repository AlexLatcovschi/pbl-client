import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShareService {
  changedTextOfRecovery = new Subject();
  showDeleteUserModal = new Subject();
  showEditUserModal = new Subject();
  showAddContribution = new Subject();

  constructor() {
  }

  sendTextOfRecovery(text: any) {
    this.changedTextOfRecovery.next(text);
  }

  getTextOfRecovery(): Observable<any> {
    return this.changedTextOfRecovery.asObservable();
  }

  sendShowDeleteUserModal(contributorId) {
    this.showDeleteUserModal.next(contributorId);
  }

  getEditUserModal(): Observable<any> {
    return this.showEditUserModal.asObservable();
  }

  sendShowEditUserModal(params) {
    this.showEditUserModal.next(params);
  }

  getDeleteUserModal(): Observable<any> {
    return this.showDeleteUserModal.asObservable();
  }

  sendAddContribution(params) {
    this.showAddContribution.next(params);
  }

  getAddContribution(): Observable<any> {
    return this.showAddContribution.asObservable();
  }
}
