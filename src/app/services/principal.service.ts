import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {AccountService} from '../core/auth/account.service';
import {AuthServerProvider} from './auth-jwt.service';

@Injectable({providedIn: 'root'})
export class Principal {
  private userIdentity: any;
  private authenticated = false;
  private authenticationState = new Subject<any>();

  constructor(
    private account: AccountService,
    private authService: AuthServerProvider) {
  }

  authenticate(identity: any) {
    this.userIdentity = identity;
    this.authenticated = identity !== null;
    this.authenticationState.next(this.userIdentity);
  }

  hasAnyAuthority(authorities: string[]): Promise<boolean> {
    return Promise.resolve(this.hasAnyAuthorityDirect(authorities));
  }

  hasAnyAuthorityDirect(authorities: string[]): boolean {
    if (!this.authenticated || !this.userIdentity || !this.userIdentity.authorities) {
      return false;
    }

    if (!authorities || !authorities.length) {
      return true;
    }

    for (const authority of authorities) {
      if (this.userIdentity.authorities.includes(authority)) {
        return true;
      }
    }

    return false;
  }

  hasAuthority(authority: string): Promise<boolean> {
    if (!this.authenticated) {
      return Promise.resolve(false);
    }

    return this.identity().then(
      id => {
        return Promise.resolve(id.authorities && id.authorities.includes(authority));
      },
      () => {
        return Promise.resolve(false);
      }
    );
  }

  identity(force?: boolean): Promise<any> {
    if (force === true) {
      this.userIdentity = undefined;
    }
    if (this.userIdentity) {
      return Promise.resolve(this.userIdentity);
    }


    if ( this.authService.getToken() ) {
      return this.account
        .get()
        .toPromise()
        .then(response => {
          const account = response.body;
          // account.blocked = account.authorities.includes('BLOCKED');    need to be resolved
          if (account) {
            this.userIdentity = account;
            this.authenticated = true;
          } else {
            this.userIdentity = null;
            this.authenticated = false;
          }
          this.authenticationState.next(this.userIdentity);
          return this.userIdentity;
        })
        .catch(err => {
          this.userIdentity = null;
          this.authenticated = false;
          this.authenticationState.next(this.userIdentity);
          return null;
        });
    } else {
      return Promise.resolve();
    }
  }

  isAuthenticated(): boolean {
    return this.authenticated;
  }

  getAuthenticationState(): Observable<any> {
    return this.authenticationState.asObservable();
  }
}
