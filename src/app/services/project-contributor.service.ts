import {Injectable} from '@angular/core';
import {HttpGenericService} from '../core/generics/http-generic.service';
import {ProjectContributorModel} from '../models/project-contributor.model';
import {map} from 'rxjs/operators';
import {SERVER_API_URL} from '../app.constants';
import {ContributionModel} from '../models/contribution.model';

@Injectable({
  providedIn: 'root'
})
export class ProjectContributorService extends HttpGenericService<ProjectContributorModel> {
  resourceUrl = 'contributors';

  public getContributor(contributorId: number) {
    return this.http.get(`${SERVER_API_URL}/api/${this.resourceUrl}/${contributorId}`)
      .pipe(map((data) => ProjectContributorModel.mapData(data)));
  }

  public getContributions(contributorId: number) {
    return this.http.get(`${SERVER_API_URL}/api/${this.resourceUrl}/${contributorId}/contributions`)
      .pipe(map((data) => ContributionModel.mapMultipleData(data)));
  }

  public getAllContributors() {
    return this.http.get(`${SERVER_API_URL}/api/${this.resourceUrl}`);
  }

  public addContribution(params) {
    return this.http.post(`${SERVER_API_URL}/api/contributions`, params);

  }

  getMyInvestmentsData() {
    return this.http.get(`${SERVER_API_URL}/api/profile/contributions`).pipe(map((data) => ContributionModel.mapMultipleData(data)));
  }
}
