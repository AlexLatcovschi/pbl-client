import {Injectable} from '@angular/core';
import {HttpGenericService} from '../core/generics/http-generic.service';
import {InvestmentModel} from '../models/investment.model';

@Injectable({
  providedIn: 'root'
})
export class InvestmentService extends HttpGenericService<InvestmentModel> {
  resourceUrl = 'investments';
}
