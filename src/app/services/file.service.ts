import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class FileService {
  constructor(private http: HttpClient) {
  }

  public deleteFile(filepath) {
    return this.http.put(`${environment.serverUrl}/api/public/uploads/users/images`, {path: filepath});
  }
  public deleteProjectFile(filepath) {
    return this.http.put(`${environment.serverUrl}/api/public/uploads/projects/images`, {path: filepath});
  }
}
