import {Injectable} from '@angular/core';
import {HttpGenericService} from '../core/generics/http-generic.service';
import {NotificationModel} from '../models/notification.model';

@Injectable({
  providedIn: 'root'
})
export class NotificationService extends HttpGenericService<NotificationModel> {
  resourceUrl = 'notifications';
}
