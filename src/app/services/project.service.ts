import {Injectable} from '@angular/core';
import {HttpGenericService} from '../core/generics/http-generic.service';
import {environment} from '../../environments/environment';
import {ProjectModel} from '../models/project';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {HttpResponse} from '@angular/common/http';
import {UserModel} from '../models/user.model';
import {SERVER_API_URL} from '../app.constants';
import {ProjectContributorModel} from '../models/project-contributor.model';

@Injectable({
  providedIn: 'root'
})
export class ProjectService extends HttpGenericService<ProjectModel> {
  resourceUrl = 'projects';

  public uploadImage(image) {
    const formData = new FormData();
    formData.append('image', image);
    return this.http.post(`${environment.serverUrl}/api/upload/projects/image`, formData, {
      reportProgress: true,
      observe: 'events',
    });
  }

  public addFounder(userId, projectId) {
    const dto = {userId};
    return this.http.put(`${environment.serverUrl}/api/projects/${projectId}/contributor`, dto);
  }

  public getMyProjects() {
    return this.http
      .get(`${environment.serverUrl}/api/profile/projects/`)
      .pipe(map((data: []) => ProjectModel.mapMultipleData(data)));
  }

  getProjectBySlug(slug: string) {
    return this.http
      .get(`${environment.serverUrl}/api/projects/slug/${slug}`)
      .pipe(map((data: []) => ProjectModel.mapMultipleData(data)));
  }

  getProjectById(id: number) {
    return this.http
      .get(`${environment.serverUrl}/api/projects/id/${id}`)
      .pipe(map((data: []) => ProjectModel.mapMultipleData(data)));
  }

  getUsers(params): Observable<HttpResponse<UserModel[]>> {
    return this.http.get<UserModel[]>(this.queryParams(params), {observe: 'response'});
  }

  queryParams(params) {
    let query = `${environment.serverUrl}/api/users?`;
    if (params.projectId) {
      query += `projectId=${params.projectId}&`;
    }
    if (params.name) {
      query += `search=${params.name}&`;
    }
    if (params.page) {
      query += `page=${params.page}&`;
    }
    if (params.size) {
      query += `size=${params.size}&`;
    }
    return query;
  }

  public deleteContributor(contributorId, projectId) {
    return this.http.delete(`${environment.serverUrl}/api/projects/${projectId}/contributor/${contributorId}`);
  }

  public getContributor(params) {
    return this.http.get(`${SERVER_API_URL}/api/${this.resourceUrl}/${params.projectSlug}/contributors/${params.contributorId}`)
      .pipe(map((data) => ProjectContributorModel.mapData(data)));
  }

  public getContributorsOfProject(projectId: number) {
    return this.http.get(`${SERVER_API_URL}/api/${this.resourceUrl}/${projectId}/contributors/`)
      .pipe(map((data) => ProjectContributorModel.mapMultipleData(data)));

  }
}
