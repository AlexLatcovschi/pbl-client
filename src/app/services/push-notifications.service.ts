import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {SwPush} from '@angular/service-worker';
import {SERVER_API_URL} from '../app.constants';
import {environment} from '../../environments/environment';
import {JhiEventManager} from './event-manager.service';
import {Principal} from './principal.service';

export const PUSH_NOTIFICATIONS_CHECK_SUBSCRIBER_KEY = 'push_notifications_check_subscriber_key';

@Injectable()
export class PushNotificationService {

  constructor(private readonly swPush: SwPush,
              private readonly http: HttpClient,
              private readonly eventManager: JhiEventManager,
              private readonly principal: Principal) {
  }

  init(): void {
    this.eventManager.subscribe(PUSH_NOTIFICATIONS_CHECK_SUBSCRIBER_KEY, () => {
      this.requestPermission();
    });
    this.requestPermission();
  }

  public requestPermission() {
    this.principal.identity().then(value => {
      if (value) {
        this.swPush.requestSubscription({serverPublicKey: environment.VAPID_PUBLIC_KEY})
          .then(sub => this.http.post(SERVER_API_URL + 'api/push-notifications', sub))
          .catch(err => console.error('Could not subscribe to notifications', err));
      }
    });
  }
}
