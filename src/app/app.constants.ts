import {environment} from '../environments/environment';

export enum AUTHORITY {
  USER = 'USER',
  USER_MGMT = 'USER_MGMT',
  ROLE_MGMT = 'ROLE_MGMT',
}

export const AUTHORITIES = [
  AUTHORITY.USER_MGMT,
  AUTHORITY.ROLE_MGMT,
];

export enum ROLE {
  ROLE_USER = 'ROLE_USER',
  ROLE_ADMIN = 'ROLE_ADMIN',
}

export const USER_ROLES = [ROLE.ROLE_ADMIN, ROLE.ROLE_USER];

export const APP_AUTH_TOKEN_KEY = 'eqAuthToken';
export const APP_BROWSER_LANG_KEY = 'browserLanguage';
export const SERVER_API_URL = environment.serverUrl;
export const IMAGE_PREFIX = environment.serverUrl + '/api/';
