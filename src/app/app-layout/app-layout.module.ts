import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {LandingModule} from '../shared-components/landing/landing-components/landing.module';
import {AppLayoutComponent} from './app-layout.component';
import {RouterModule} from '@angular/router';
import {AccountLayoutModule} from './account/account-layout.module';
import {ApplicationModule} from './application/application.module';

@NgModule({
  declarations: [
    AppLayoutComponent,
  ],
  imports: [
    BrowserModule,
    LandingModule,
    AccountLayoutModule,
    ApplicationModule,
    RouterModule,
  ],
})
export class AppLayoutModule {
}
