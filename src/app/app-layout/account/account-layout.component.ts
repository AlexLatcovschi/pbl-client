import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ShareService} from '../../services/share.service';

@Component({
  selector: 'app-account-layout',
  templateUrl: './account-layout.component.html',
  styleUrls: ['./account-layout.component.scss']
})
export class AccountLayoutComponent implements OnInit {
  header = 'Login';
  subheader;
  link = {
    text: '',
    routerlink: ''
  };

  constructor(
    private router: Router,
    private shareService: ShareService
  ) {
    this.initText(this.router.url);
    this.router.events.subscribe(data => {
      this.initText(this.router.url);
    });
  }

  ngOnInit() {
  }

  initText(url) {
    switch (url) {
      case '/account/login':
        this.header = 'Login';
        this.subheader = 'Welcome back! Login to acces Equivallent platform.';
        this.link.text = 'Did you forget your password?';
        this.link.routerlink = 'reset-password';
        break;
      case '/account/register':
        this.header = 'Register';
        this.subheader = 'Register now, to acces Equivallent platform. Do you ';
        this.link.text = 'have an account?';
        this.link.routerlink = 'login';
        break;
      case '/account/reset-password':
        this.header = 'Reset Password';
        this.subheader = 'Reset your password. Please enter your email and we will send you a recover link.';
        this.link.text = '';
        this.link.routerlink = '';
        this.shareService.getTextOfRecovery().subscribe(data => {
          if (data) {
            this.header = data.header;
            this.subheader = data.subheader;
            this.link = data.link;
          }
        });
        break;
    }

  }

}
