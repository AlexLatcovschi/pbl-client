import {Component, OnInit} from '@angular/core';
import {ShareService} from '../../../../services/share.service';

@Component({
  selector: 'app-reset-password-page',
  templateUrl: './reset-password-page.component.html',
  styleUrls: ['./reset-password-page.component.scss']
})
export class ResetPasswordPageComponent implements OnInit {
  shouldWriteEmail;
  wasClickedSendEmail;
  wasConfirmedFromEmail;
  successfullyChangedPassword;
  text = {
    header: '',
    subheader: '',
    link: null
  };

  constructor(
    private shareService: ShareService
  ) {
    this.text.link = {
      text: '',
      routerlink: ''
    };
  }

  ngOnInit() {
    this.shouldWriteEmail = true;
    this.wasConfirmedFromEmail = false;
    this.wasClickedSendEmail = false;
    this.successfullyChangedPassword = false;
  }

  sendEmail() {
    this.shouldWriteEmail = false;
    this.wasConfirmedFromEmail = false;
    this.successfullyChangedPassword = false;
    this.wasClickedSendEmail = true;
    this.text.header = 'Check email';
    this.text.subheader = 'We’ve been sent and recovery link on your email:' +
      ' ropotv@gmail.com. If you didn’t receive any email, check your spam folder. If that didn’t work, please send again in 54 secs.';
    this.shareService.sendTextOfRecovery(this.text);
  }

  sendBetaConfirmed() {
    this.shouldWriteEmail = false;
    this.wasClickedSendEmail = false;
    this.successfullyChangedPassword = false;
    this.wasConfirmedFromEmail = true;
    this.text.header = 'Reset Password';
    this.text.subheader = 'Please enter a new password';
    this.shareService.sendTextOfRecovery(this.text);
  }

  sendSuccess() {
    this.shouldWriteEmail = false;
    this.wasClickedSendEmail = false;
    this.wasConfirmedFromEmail = false;
    this.successfullyChangedPassword = true;
    this.text.header = 'Success';
    this.text.subheader = 'You have been changed your password successfully! Please,';
    this.text.link.text = 'login here.';
    this.text.link.routerlink = '/account/login';
    this.shareService.sendTextOfRecovery(this.text);
  }
}
