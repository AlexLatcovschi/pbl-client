import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {environment} from '../../../../../environments/environment';
import {AuthService} from '../../../../core/auth/auth.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  serverUrl = environment.serverUrl;
  errorMessage: string;
  serverError = false;
  token;
  redirectLink = '/app';

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
  }

  get f() {
    return this.loginForm.controls;
  }

  ngOnInit() {
    const token = 'token';
    this.route.queryParams.subscribe(params => {
      this.token = params[token];
      if (this.token) {
        this.loginWithToken();
      }
    });

    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern('^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$')]],
    });
  }

  login() {
    this.submitted = true;

    if (this.loginForm.valid) {
      this.authService.login(this.loginForm.value).subscribe(
        res => {
          this.serverError = false;
          if (res) {
            this.router.navigate([this.redirectLink]);
          }
        },
        error => {
          this.serverError = true;
          if (error.error.message === 'EmailNotExists') {
            this.errorMessage = 'User_With_This_Email_Not_found';
          } else if (error.error.message === 'InvalidPassword') {
            this.errorMessage = 'IncorrectPassword';
          } else {
            this.errorMessage = 'AnErrorOccured';
          }
        },
      );
    }
  }

  loginWithToken() {
    this.authService.loginWithToken(this.token).subscribe(
      () => {
      },
      () => {
      },
      () => {
        this.router.navigate([this.redirectLink]);
      },
    );
  }

}
