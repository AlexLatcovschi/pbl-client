import {Component, OnInit} from '@angular/core';
import {UserModel} from '../../../../models/user.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {environment} from '../../../../../environments/environment';
import {ActivatedRoute, Router} from '@angular/router';
import {MustMatch} from '../../../../Validators/match-validator';
import {AuthService} from '../../../../core/auth/auth.service';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss']
})
export class RegisterPageComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  user: UserModel;
  serverError = false;
  serverUrl = environment.serverUrl;
  token;
  serverRegisterError = false;
  serverRegisterErrorMessage = '';
  checkedPassword: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
  }

  get f() {
    return this.registerForm.controls;
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.token = params['token'];
      if (this.token) {
        this.registerWithToken();
      }
    });
    // Validators.pattern('^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$')
    this.registerForm = this.formBuilder.group(
      {
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.pattern('^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$')]],
        confirmPassword: ['', Validators.required],
      },
      {
        validators: MustMatch('password', 'confirmPassword'),
      },
    );
  }

  registerUser() {
    this.submitted = true;
    this.serverRegisterError = false;
    this.serverError = false;
    if (this.registerForm.valid) {
      this.user = new UserModel();
      this.user.email = this.registerForm.value.email.trim();
      this.user.password = this.registerForm.value.password.trim();
      this.authService.register(this.user).subscribe(
        (registerRes: any) => {
          const registerToken = registerRes.token;

          if (registerToken) {
            this.authService
              .login({
                email: this.user.email,
                password: this.user.password,
              })
              .subscribe(
                loginRes => {
                  this.serverError = false;
                  if (loginRes) {
                    this.router.navigate(['/app']);
                  }
                },
                error => {
                  this.serverError = true;
                },
              );
          }
        },
        error => {
          this.serverRegisterError = true;
          this.serverRegisterErrorMessage = error.error.message;
        },
      );
    }
  }

  registerWithToken() {
    this.authService.loginWithToken(this.token).subscribe(
      () => {
      },
      () => {
      },
      () => {
        this.router.navigate(['/']);
      },
    );
  }

  checkPassword() {
    this.checkedPassword = true;
  }
}
