import {Routes} from '@angular/router';
import {LoginPageComponent} from './login-page/login-page.component';
import {RegisterPageComponent} from './register-page/register-page.component';
import {ResetPasswordPageComponent} from './reset-password-page/reset-password-page.component';
import {RegisterGuard} from '../../../core/guards/register.guard';

export const accountPagesRoute: Routes = [
  {
    path: 'login',
    component: LoginPageComponent,
  },
  {
    path: 'register',
    component: RegisterPageComponent,
    canActivate: [RegisterGuard],
  },
  {
    path: 'reset-password',
    component: ResetPasswordPageComponent,
  },
];
