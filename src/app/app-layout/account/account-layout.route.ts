import {Routes} from '@angular/router';
import {AccountLayoutComponent} from './account-layout.component';
import {accountPagesRoute} from './account-pages/account-pages.route';

const ACCOUNT_ROUTES = [...accountPagesRoute];

export const accountLayoutRoute: Routes = [
  {
    path: 'account',
    component: AccountLayoutComponent,
    children: ACCOUNT_ROUTES
  },
];
