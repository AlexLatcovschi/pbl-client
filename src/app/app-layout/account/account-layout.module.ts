import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {AccountLayoutComponent} from './account-layout.component';
import {LoginPageComponent} from './account-pages/login-page/login-page.component';
import {NavbarModule} from '../../shared-components/landing/navbar/navbar.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RegisterPageComponent } from './account-pages/register-page/register-page.component';
import { ResetPasswordPageComponent } from './account-pages/reset-password-page/reset-password-page.component';

@NgModule({
  imports: [CommonModule, RouterModule, NavbarModule, FormsModule, ReactiveFormsModule],
  declarations: [
    AccountLayoutComponent,
    LoginPageComponent,
    RegisterPageComponent,
    ResetPasswordPageComponent
  ],
  exports: [
    AccountLayoutComponent,
  ],
})
export class AccountLayoutModule {
}
