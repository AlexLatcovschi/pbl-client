import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-app-layout',
  templateUrl: './app-layout.component.html',
  styleUrls: ['./app-layout.component.scss']
})
export class AppLayoutComponent implements OnInit {
  isLandingPage;

  constructor(
    private router: Router,
  ) {
    this.checkIfLanding();
  }

  ngOnInit() {
    this.router.events.subscribe(data => {
      this.checkIfLanding();
    });
  }

  checkIfLanding() {
    this.isLandingPage = this.router.url === '/';
  }
}
