import {Component, OnInit} from '@angular/core';
import {UserDataService} from '../../core/auth/user-data.service';
import {UserModel} from '../../models/user.model';
import {ShareService} from '../../services/share.service';

@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.scss']
})
export class ApplicationComponent implements OnInit {
  user: UserModel;
  zIndex = 11;
  showModal;
  deleteContributorModal;
  editContributorModal;
  addContributionModal;

  constructor(
    private userService: UserDataService,
    private sharedService: ShareService
  ) {
  }

  ngOnInit() {
    this.getProfile();
    this.updateDeleteUser();
    this.updateEditUser();
    this.updateAddContribution();
  }

  getProfile() {
    this.userService.getUser().subscribe(data => {
      this.user = data;
    });
  }

  openModal() {
    this.showModal = true;
  }

  updateShowModal(event) {
    this.showModal = event;
  }

  updateDeleteUser() {
    this.sharedService.getDeleteUserModal().subscribe(data => {
      this.deleteContributorModal = data;
    });
  }

  updateEditUser() {
    this.sharedService.getEditUserModal().subscribe(data => {
      this.editContributorModal = data;
    });
  }

  updateAddContribution() {
    this.sharedService.getAddContribution().subscribe(data => {
      console.log(data);
      this.addContributionModal = data;
    });
  }

  updateDeleteModal() {
    this.deleteContributorModal = null;
  }

  updateEditContributorModal() {
    this.editContributorModal = null;
  }

  updateAddContributionModal() {
    this.addContributionModal = null;
  }
}
