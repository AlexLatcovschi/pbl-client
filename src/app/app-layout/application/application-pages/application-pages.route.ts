import {Routes} from '@angular/router';
import {AppHomePageComponent} from './app-home-page/app-home-page.component';
import {AppProjectsPageComponent} from './app-projects-page/app-projects-page.component';
import {AppEditAccountPageComponent} from './app-edit-account-page/app-edit-account-page.component';
import {AppSingleProjectPageComponent} from './app-single-project-page/app-single-project-page.component';
import {AppUserPageComponent} from './app-user-page/app-user-page.component';
import {AppEditProjectPageComponent} from './app-edit-project-page/app-edit-project-page.component';
import {AppNotificationsComponent} from './app-notifications/app-notifications.component';

export const applicationPagesRoutes: Routes = [
  {
    path: '',
    component: AppHomePageComponent,
  },
  {
    path: 'projects',
    component: AppProjectsPageComponent,
  },
  {
    path: 'profile',
    component: AppEditAccountPageComponent,
  },
  {
    path: 'project/:slug',
    component: AppSingleProjectPageComponent,
  },
  {
    path: 'project/:slug/contributor/:id',
    component: AppUserPageComponent,
  },
  {
    path: 'project/:slug/edit',
    component: AppEditProjectPageComponent,
  },
  {
    path: 'notifications',
    component: AppNotificationsComponent,
  },
];
