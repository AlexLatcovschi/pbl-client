import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, RouterLink} from '@angular/router';

@Component({
  selector: 'app-app-edit-project-page',
  templateUrl: './app-edit-project-page.component.html',
  styleUrls: ['./app-edit-project-page.component.scss']
})
export class AppEditProjectPageComponent implements OnInit {
  projectSlug: string;

  constructor(
    private route: ActivatedRoute
  ) {
    this.route.params.subscribe(params => {
      this.projectSlug = params.slug;
    });
  }

  ngOnInit() {

  }

}
