import {Component, OnInit} from '@angular/core';
import {ProjectService} from '../../../../services/project.service';
import {ProjectModel} from '../../../../models/project';

@Component({
  selector: 'app-app-projects-page',
  templateUrl: './app-projects-page.component.html',
  styleUrls: ['./app-projects-page.component.scss']
})
export class AppProjectsPageComponent implements OnInit {
  public projects: ProjectModel[];

  constructor(
    private projectService: ProjectService
  ) {
  }

  ngOnInit() {
    this.getProjects();
  }

  getProjects() {
    this.projectService.getMyProjects().subscribe(data => {
      this.projects = data;
    });
  }
}
