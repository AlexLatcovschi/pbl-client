import {Component, OnInit} from '@angular/core';
import {UserDataService} from '../../../../core/auth/user-data.service';
import {UserModel} from '../../../../models/user.model';
import {ProjectContributorService} from '../../../../services/project-contributor.service';
import {ContributionModel} from '../../../../models/contribution.model';

@Component({
  selector: 'app-app-home-page',
  templateUrl: './app-home-page.component.html',
  styleUrls: ['./app-home-page.component.scss']
})
export class AppHomePageComponent implements OnInit {
  user: UserModel;
  contributions: ContributionModel[];

  constructor(
    private userService: UserDataService,
    private contributorService: ProjectContributorService
  ) {
  }

  ngOnInit() {
    this.userService.getUser().subscribe(data => {
      this.user = data;
    });
  }

  getDatas() {
    this.contributorService.getMyInvestmentsData().subscribe(data => {
      this.contributions = data;
    });
  }
}
