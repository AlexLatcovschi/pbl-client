import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProjectContributorModel} from '../../../../models/project-contributor.model';
import {ProjectService} from '../../../../services/project.service';

@Component({
  selector: 'app-app-user-page',
  templateUrl: './app-user-page.component.html',
  styleUrls: ['./app-user-page.component.scss']
})
export class AppUserPageComponent implements OnInit {
  contributor: ProjectContributorModel;

  constructor(
    private route: ActivatedRoute,
    private contributorService: ProjectService
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(data => {
      if (data.id && data.slug) {
        const params = {
          contributorId: data.id,
          projectSlug: data.slug,
        };
        this.getContributor(params);
      }
    });
  }

  getContributor(params) {
    this.contributorService.getContributor(params).subscribe(data => {
      this.contributor = data;
    });
  }
}
