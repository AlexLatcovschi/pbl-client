import {Component, OnInit} from '@angular/core';
import {ProjectService} from '../../../../services/project.service';
import {ActivatedRoute} from '@angular/router';
import {ProjectModel} from '../../../../models/project';
import {ProjectContributorModel} from '../../../../models/project-contributor.model';

@Component({
  selector: 'app-app-single-project-page',
  templateUrl: './app-single-project-page.component.html',
  styleUrls: ['./app-single-project-page.component.scss']
})
export class AppSingleProjectPageComponent implements OnInit {
  projectSlug: string;
  project: ProjectModel;
  contributors: ProjectContributorModel[];

  constructor(
    private projectService: ProjectService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(data => {
      if (data.slug) {
        this.projectSlug = data.slug;
        this.initProject();
      }
    });
  }

  initProject() {
    this.projectService.getProjectBySlug(this.projectSlug).subscribe(data => {
      this.project = data[0];
      this.getContributors();
    });
  }

  getContributors() {
    this.projectService.getContributorsOfProject(this.project.id).subscribe(data => {
      this.contributors = data;
      console.log(this.contributors);
    });
  }
}
