import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {ApplicationComponent} from './application.component';
import {CommonModule} from '@angular/common';
import {AppHomePageComponent} from './application-pages/app-home-page/app-home-page.component';
import { AppProjectsPageComponent } from './application-pages/app-projects-page/app-projects-page.component';
import { AppSingleProjectPageComponent } from './application-pages/app-single-project-page/app-single-project-page.component';
import { AppEditProjectPageComponent } from './application-pages/app-edit-project-page/app-edit-project-page.component';
import { AppUserPageComponent } from './application-pages/app-user-page/app-user-page.component';
import { AppEditAccountPageComponent } from './application-pages/app-edit-account-page/app-edit-account-page.component';
import {AppHeaderModule} from '../../shared-components/application/app-header/app-header.module';
import {AppSidebarModule} from '../../shared-components/application/app-sidebar/app-sidebar.module';
import {BlocksModule} from '../../shared-components/application/blocks/blocks.module';
import {ModalModule} from '../../shared-components/general/modal/modal.module';
import {BulletBtnModule} from '../../shared-components/application/bullet-btn/bullet-btn.module';
import { AppNotificationsComponent } from './application-pages/app-notifications/app-notifications.component';

@NgModule({
  declarations: [
    ApplicationComponent,
    AppHomePageComponent,
    AppProjectsPageComponent,
    AppSingleProjectPageComponent,
    AppEditProjectPageComponent,
    AppUserPageComponent,
    AppEditAccountPageComponent,
    AppNotificationsComponent,
  ],
  imports: [
    RouterModule,
    CommonModule,
    AppHeaderModule,
    AppSidebarModule,
    BlocksModule,
    ModalModule,
    BulletBtnModule
  ],
})
export class ApplicationModule {
}
