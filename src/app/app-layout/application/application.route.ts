import {Routes} from '@angular/router';
import {ApplicationComponent} from './application.component';
import {applicationPagesRoutes} from './application-pages/application-pages.route';
import {AuthGuard} from '../../core/guards/auth.guard';

const APPLICATION_ROUTES = [...applicationPagesRoutes];

export const applicationRoutes: Routes = [
  {
    path: 'app',
    component: ApplicationComponent,
    children: APPLICATION_ROUTES,
    canActivate: [AuthGuard],

  },
];
