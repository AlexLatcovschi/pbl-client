import {Routes} from '@angular/router';
import {AppLayoutComponent} from './app-layout.component';
import {accountLayoutRoute} from './account/account-layout.route';
import {landingRoute} from './landing-page/landing-page.route';
import {applicationRoutes} from './application/application.route';

const LAYAOUT_ROUTES = [...accountLayoutRoute, ...landingRoute, ...applicationRoutes];

export const appLayoutRoutes: Routes = [
  {
    path: '',
    component: AppLayoutComponent,
    children: LAYAOUT_ROUTES,
  },
];
