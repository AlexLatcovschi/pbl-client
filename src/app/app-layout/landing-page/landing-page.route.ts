import {Routes} from '@angular/router';
import {LandingPageComponent} from './landing-page.component';

export const landingRoute: Routes = [
  {
    path: '',
    component: LandingPageComponent,
  },
];
