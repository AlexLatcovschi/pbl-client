import {Component, OnInit} from '@angular/core';
import {UserDataService} from '../../core/auth/user-data.service';
import {UserModel} from '../../models/user.model';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {
  user: UserModel;

  constructor(
    private userService: UserDataService
  ) {
  }

  ngOnInit() {
    this.initUser();
  }

  initUser() {
    this.userService.getUser().subscribe(data => {
      this.user = data;
      if (this.user) {
        console.log('Go to user');
      }
    });
  }
}
