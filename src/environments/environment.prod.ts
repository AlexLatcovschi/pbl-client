export const environment = {
  production: true,
  development: false,
  frontUrl: 'http://localhost:4200',
  serverUrl: 'http://localhost:3000',
  VAPID_PUBLIC_KEY: 'BNxhwBAXM1zHDF1arJgJSOLp2KU0IDdh6RlPmV0Z3YgpOvxMSxMbWBq3ZvlZ5QMtLyqcYAdcpwhX9vHNGt6ZwY0',
};
