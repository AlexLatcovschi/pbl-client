importScripts('./ngsw-worker.js');

self.addEventListener('notificationclick', function(event) {
  event.notification.close();
  if (event && event.notification && event.notification.data && event.notification.data.redirectUrl) {
    // clients.openWindow('/' + event.notification.data.redirectUrl);
    event.waitUntil(clients.matchAll({
      type: "window"
    }).then(function(clientList) {
      for (let i = 0; i < clientList.length; i++) {
        const client = clientList[i];
        if (client.url == ('/' + event.notification.data.redirectUrl) && 'focus' in client) {
          return client.focus();
        }
      }
      if (clients.openWindow)
        return clients.openWindow('/' + event.notification.data.redirectUrl);
    }));
  }
}, false);
